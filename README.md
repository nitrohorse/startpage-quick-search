# StartPage Quick Search

[![License](https://img.shields.io/badge/licensed-ethically-%234baaaa)](https://gitlab.com/nitrohorse/startpage-quick-search/blob/master/LICENSE.md)
[![npm](https://img.shields.io/npm/v/startpage-quick-search.svg)](https://www.npmjs.com/package/startpage-quick-search)
[![Downloads](https://img.shields.io/npm/dt/startpage-quick-search.svg)](https://www.npmjs.com/package/startpage-quick-search)

Get the top 10 search results from StartPage.com in your terminal.

## Installation

``` bash
npm i -g startpage-quick-search
```
## Usage

Just type `spage` and then your query!

### Example
```bash
spage who is reality winner
```

![Search Results](https://gitlab.com/nitrohorse/startpage-quick-search/raw/master/screenshot.png)

## npm Dependencies
* [chalk](https://www.npmjs.com/package/chalk)
* [ora](https://www.npmjs.com/package/ora)
* [scrape-it](https://www.npmjs.com/package/scrape-it)

## Develop Locally
* Clone the repository
* `cd` into the cloned directory
* Install tools:
	* [Node.js](https://nodejs.org/en/)
* Install dependencies: 
	* `npm i`
* Search:
  * `npm start <query>`
