#!/usr/bin/env node

const chalk = require('chalk')
const startPageQuickSearch = require('../lib/StartPageQuickSearch')

startPageQuickSearch
	.run()
	.catch(err => {
		console.error(chalk.bold.red('Oops!', err))
		process.exit(1)
	})
