'use strict'

const chalk = require('chalk')
const scrapeIt = require('scrape-it')
const ora = require('ora')
const spinner = ora()

const StartPageQuickSearch = (() => {
	const printSearchResults = searchResults => {
		for (let index = searchResults.length - 1; index >= 0; index--) {
			const searchResult = searchResults[index]
			console.log(chalk`
 {blue.bold ${index + 1}.} {cyan.bold ${searchResult.title}}
 {yellow.bold ${searchResult.url}}
 ${searchResult.description}
 {gray ${searchResult.proxy}}`
			)
		}
	}

	const getSearchResults = async (query) => {
		const response = await scrapeIt(`https://www.startpage.com/do/search?q=${query}`, {
			searchResults: {
				listItem: 'div.w-gl__result',
				data: {
					title: 'a.w-gl__result-title',
					url: {
						selector: 'a.w-gl__result-url',
						attr: 'href'
					},
					description: 'p.w-gl__description',
					proxy: {
						selector: 'a.w-gl__anonymous-view-url',
						attr: 'href'
					}
				}
			}
		})
		return response.data.searchResults
	}

	const displayWarningAndExit = warning => {
		console.warn(chalk.yellow(warning))
		process.exit(0)
	}

	const getQueryFromCommandLine = () => {
		let args = []
		for (let index = 2; index < process.argv.length; index++) {
			const arg = process.argv[index]
			args.push(arg)
		}
		const query = args.join(' ')

		if (!query.length || process.argv.length < 2) {
			displayWarningAndExit('You forgot to enter a query!')
		}
		return query
	}

	const run = async () => {
		const query = getQueryFromCommandLine()

		spinner.text = `Getting search results for "${query}"...`
		spinner.start()

		try {
			const searchResults = await getSearchResults(query)

			if (!searchResults.length) {
				spinner.stop()
				displayWarningAndExit('Sorry, there are no Web results for this search! If this continues to occur please file an issue at https://gitlab.com/nitrohorse/startpage-quick-search/-/issues.')
			}

			spinner.stop()
			printSearchResults(searchResults)
		} catch (err) {
			console.error(chalk.bold.red('Oops!', err))
		}
	}

	return {
		run
	}
})()

module.exports = StartPageQuickSearch
